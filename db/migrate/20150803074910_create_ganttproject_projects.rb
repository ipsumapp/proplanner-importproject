class CreateGanttprojectProjects < ActiveRecord::Migration
  def change
    create_table :ganttproject_projects do |t|
      t.string :name
      t.string :file_name

      t.timestamps null: false
    end
  end
end
