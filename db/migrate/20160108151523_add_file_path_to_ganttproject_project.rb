class AddFilePathToGanttprojectProject < ActiveRecord::Migration
  def change
    add_column :ganttproject_projects, :file_path, :string
  end
end
