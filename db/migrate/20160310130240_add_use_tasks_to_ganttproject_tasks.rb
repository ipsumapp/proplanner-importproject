class AddUseTasksToGanttprojectTasks < ActiveRecord::Migration
  def change
    add_column :ganttproject_tasks, :use_tasks, :boolean, default: true
  end
end
