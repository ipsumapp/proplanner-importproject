class CreateHistorical < ActiveRecord::Migration
  def change
    create_table :ganttproject_historicals do |t|
      t.integer :sector_id
      t.float :progress
      t.float :expected_progress
      t.integer :project_id
      t.integer :task_id
      t.timestamps null: false
    end
    add_index :ganttproject_historicals, :sector_id
    add_index :ganttproject_historicals, :project_id
    add_index :ganttproject_historicals, :task_id
  end
end
