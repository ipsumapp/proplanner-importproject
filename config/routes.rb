Ganttproject::Engine.routes.draw do

  resources :projects do
    resources :resources
    resources :tasks
    resources :links
  end
  root :to => 'projects#index'

  match "ganttproject/projects/datagrid", :to => "projects#datagrid", :as => "data_grid", :via => "get"
  match "ganttproject/projects/datagrid_action", :to => "projects#datagrid_action", :as => "datagrid_action", :via => "get"
  match "ganttproject/projects/:id/datagantt", :to => "projects#datagantt", :as => "data_gantt", :via => "get"
  match "ganttproject/projects/:id/datagantt_action", :to => "projects#datagantt_action", :as => "datagantt_action", :via => "post"

end
