# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151215192427) do

  create_table "ganttproject_assignments", force: :cascade do |t|
    t.integer  "task_id"
    t.integer  "resource_id"
    t.date     "start"
    t.date     "finish"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "ganttproject_links", force: :cascade do |t|
    t.integer  "source"
    t.integer  "target"
    t.integer  "gtype"
    t.integer  "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ganttproject_projects", force: :cascade do |t|
    t.string   "name"
    t.string   "file_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ganttproject_resources", force: :cascade do |t|
    t.string   "name"
    t.string   "initials"
    t.float    "max_units"
    t.float    "cost"
    t.integer  "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ganttproject_tasks", force: :cascade do |t|
    t.string   "name"
    t.datetime "start"
    t.datetime "finish"
    t.integer  "duration"
    t.float    "progress"
    t.integer  "parent_id"
    t.integer  "project_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "ancestry"
    t.string   "guid"
    t.integer  "unique_id"
    t.integer  "childless",  default: -1
    t.string   "full_name"
  end

  add_index "ganttproject_tasks", ["ancestry"], name: "index_ganttproject_tasks_on_ancestry"

end
