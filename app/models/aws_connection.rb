class AwsConnection

  def initialize
    Aws.use_bundled_cert!
    Aws.config.update({ region: "sa-east-1", 
                        credentials: Aws::Credentials.new(ENV["AWS_ACCESS_KEY_ID"],ENV["AWS_SECRET_ACCESS_KEY"])
    })
  end

  def s3_bucket(bucket_name=ENV["S3_BUCKET"])
    Aws::S3::Resource.new.bucket(bucket_name)
  end

  def s3_client(bucket_name=ENV["S3_BUCKET"])
    
  end

  def upload_file_to_s3(filename,filepath,options={})
    options[:acl] ||= "public-read"
    object = s3_bucket.object(filename)
    object.upload_file(filepath, acl: options[:acl])
    object.public_url.gsub("https","http") rescue nil
  end


end
