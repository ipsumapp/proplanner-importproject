module Ganttproject
  class Task < ActiveRecord::Base
    has_ancestry
    has_many :assignments, dependent: :destroy
    has_many :resources, through: :assignments
    belongs_to :project, :foreign_key => "project_id"
    def from_params(params, id)
      self.name       = params["#{id}_text"].to_s
      self.start      = params["#{id}_start_date"].to_date
      self.finish      = params["#{id}_end_date"].to_date
      self.duration   = params["#{id}_duration"].to_i
      self.progress   = params["#{id}_progress"].to_i
      if params["#{id}_parent"].to_i>0
        self.parent_id     = params["#{id}_parent"].to_i
      end
      self.project_id=params[:id]

    end

    def full_name
      unless read_attribute(:full_name).present?
        name = (self.ancestors.where(project_id: self.project_id).where("ancestry is not ?",nil).map(&:name)+ [self.name]).join(" > ")
        update_attribute(:full_name,(name.present? ? name : self.name))
      end
      read_attribute(:full_name)
    end
    def calculate_values
      if self.is_childless?
        self.childless = 1
      else
        self.childless = 0
      end
    end
    def calculate_ponderators
      begin
        if self.childless == 0
          self.children.to_a.sum{|c| c.calculate_ponderators}
        else
          ponderator
        end
      rescue => ex
        puts(ex.message + "\n" + ex.backtrace.join("\n"))
        0
      end
    end
    def calculate_ponderated_progress
      begin
        if self.childless == 0
          self.children.to_a.sum{|c| c.calculate_ponderated_progress}
        else
          ponderated_progress
        end
      rescue => ex
        puts(ex.message + "\n" + ex.backtrace.join("\n"))
        0
      end
    end
    def update_progress(use_tasks=true)
      updated_tasks = {}
      self.use_tasks = use_tasks
      self.save
      self.calculate_progress
      updated_tasks[self.id] = progress
      if self.parent
        updated_tasks.merge!(self.parent.update_progress(true))
      end
      puts updated_tasks.inspect
      return updated_tasks
    end
    def ponderated_progress
      if self.use_tasks
        tasks = ::Task.select(:progress,:end_at,:start_at).where(master_unique_id: self.unique_id, sector_id: self.sector_id)
        if tasks.any?
          tasks.to_a.sum{|t| t.progress*(t.end_at.to_datetime.end_of_day.to_i - t.start_at.to_datetime.beginning_of_day.to_i)}
        else
          0
        end
      else
        self.progress*(self.finish.to_datetime.end_of_day.to_i - self.start.to_datetime.beginning_of_day.to_i)
      end
    end
    def ponderator
      if self.use_tasks
        tasks = ::Task.select(:end_at,:start_at).where(master_unique_id: self.unique_id, sector_id: self.sector_id)
        tasks_sum = 0
        if self.finish && self.start
          time_sum = self.finish.to_datetime.end_of_day.to_i - self.start.to_datetime.beginning_of_day.to_i
        else
          time_sum = 0
        end
        if tasks.any?
          tasks_sum = tasks.to_a.sum{|t| (t.end_at.end_of_day.to_time.to_i - t.start_at.beginning_of_day.to_time.to_i)}
        end
        if time_sum >= tasks_sum
          time_sum
        else
          tasks_sum
        end
      else
        (self.finish.to_datetime.end_of_day.to_i - self.start.to_datetime.beginning_of_day.to_i)
      end
    end
    def calculate_progress
      a = calculate_ponderators
      b = calculate_ponderated_progress
      a!=0 ? self.progress = (b/a.to_f).round(2) : self.progress = 0
      self.save
      
    end

    def total_vs_released_constraints
      tasks_ids = ::Task.where(master_unique_id: self.unique_id, sector_id: self.sector_id).pluck(:id)
      constraints = ::Constraint.select(:completed).unscoped.where("(task_id in (?) or master_task_id = ?) and sector_id = ?", tasks_ids, self.unique_id, self.sector_id).group(:completed).count(:id)
      released = constraints[true] || 0
      total = (constraints[false] || 0) + released
      [released, total]
    end

    def expected_progress(now=DateTime.now.to_datetime.to_i)
      begin 
        start_time = self.start.to_datetime.to_i
        end_time = self.finish.to_datetime.to_i
        expected_progress = (((now-start_time)/(end_time - start_time).to_f))*100.round(2)
        if expected_progress < 0
          0
        elsif expected_progress > 100
          100
        else
          expected_progress
        end
      rescue => ex
        100
      end
    end
    def state
      threshold = 3
      if (expected_progress - threshold) > self.progress
        "red"
      elsif expected_progress > self.progress && self.progress >= (expected_progress - threshold)
        "yellow"
      elsif expected_progress <= self.progress && self.progress < (expected_progress + threshold)
        "green"
      elsif (expected_progress + threshold) >= self.progress
        "blue"
      else
        "white"
      end
    end


    def json_data
      Rails.cache.fetch("#{self.cache_key}/json_data", expires_in: 1.day) do
        begin
          start_date = Date.parse(self.start.to_s).strftime("%d-%m-%Y")
        rescue Exception => e
          start_date = self.start
        end
        hash = {
          :id => self.id,
          :text =>  self.name,
          :start_date => start_date,
          :duration => self.duration,
          :progress => self.progress/100,
          :progress_percent => self.progress.to_s+"%",
          :parent => self.parent_id,
          :type => 'task',
          :open=> true,
          :unique_id => self.unique_id,
          :state => self.state,
          :constraints => "#{self.total_vs_released_constraints.join('/')}",
          :expected_progress => self.expected_progress,
          :childless => self.childless
        }
      end
    end

  end
end
