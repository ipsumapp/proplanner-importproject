module Ganttproject
  class Project < ActiveRecord::Base
    has_paper_trail# :ignore => [:file_name]
    has_many :tasks , dependent: :destroy
    has_many :resources, dependent: :destroy
    has_many :links, dependent: :destroy

    attr_accessor :file
    attr_accessor :task_id_hash
    before_save :change_filename_and_upload_to_s3 if :file
    

    def change_filename_and_upload_to_s3
      self.file_name =  file.original_filename
      s3_url = AwsConnection.new.upload_file_to_s3(self.file_name,file.path)
      self.file_path = s3_url
    end

    def process_project_file
      ::Project.import_from_msproject(self.id, self.file_path, self.file_name)
    end

    def import(filepath,filename)
      t1 = Time.now
      input_file = open(filepath)

      split_name = filename.split(".")
      output_file = Tempfile.new(["tmp-#{split_name.first}", ".#{split_name.last}"])

      output_file.binmode
      output_file.write input_file.read
      output_file.flush

      output_file.seek(0)

      project = MPXJ::Reader.read(output_file.path)
      project_id=self.id
      #self.update_attribute(:file_name,filename)

      resource_id_hash = Hash.new
      @task_id_hash = {}

      #project.all_resources.each do |resource|
      #  r=Ganttproject::Resource.new
      #  r.project_id=project_id
      #  r.initials=resource.initials
      #  r.max_units=resource.max_units
      #  r.cost=resource.cost
      #  r.save
      #end


      project.all_tasks.each do |task|
        task_creation(task,project_id)
      end


      #project.all_assignments.each do |assignment|
      #  a=Assignment.new
      #  a.resource_id=resource_id_hash[assignment.resource.id] if assignment.resource
      #  a.task_id=task_id_hash[assignment.task.id] if assignment.task
      #  a.start=assignment.start
      #  a.finish=assignment.finish
      #  a.save
      #end

      logger.info("Checking links...")
      project.all_tasks.each do |task|
        task.successors.each do |link|
          #logger.info("Link: #{link.guid}")
          l=Link.new
          l.gtype = link.type
          l.source = @task_id_hash[task.id]
          l.target = @task_id_hash[project.get_task_by_unique_id(link.task_unique_id).id]
          l.project_id=project_id
          case link.type
            when "FS"
              l.gtype = 0
            when "SS"
              l.gtype = 1
            when "FF"
              l.gtype = 2
            when "SF"
              l.gtype = 3
            else
              l.gtype = 0
          end
          l.save
        end


      end

      t2 = Time.now
      logger.info("Time execution: #{t2-t1} segs")

      # self.file = nil
      # update file_name: filename

    end


    def task_creation(task,project_id)
      
      t = Ganttproject::Task.find_or_initialize_by(unique_id: task.unique_id, sector_id: self.sector_id)
      t.name = task.name
      t.project_id = project_id
      t.start = task.start
      t.finish = task.finish
      begin
        t.duration = (Date.parse(task.finish.to_s) - Date.parse(task.start.to_s)).to_i
      rescue
        t.duration = task.duration
      end
      t.progress = 0 if t.progress.nil? || t.id.nil?
      t.childless = (task.child_tasks.none? ? 1 : 0)
      if task.parent_task_unique_id != -1
        t.parent_id = Ganttproject::Task.find_or_create_by(unique_id: task.parent_task_unique_id, sector_id: self.sector_id).id
      else
        t.parent_id = nil
      end
      t.guid = task.guid
      t.save
      @task_id_hash[task.id] = t.id
      
    end
  end
end
