require_dependency "ganttproject/application_controller"

module Ganttproject
  class ProjectsController < ApplicationController
    before_action :set_project, only: [:show, :edit, :update, :destroy]
    skip_before_filter :verify_authenticity_token

    # GET /projects
    def index
      #@projects = Ganttproject::Project.all
    end

    # GET /projects/1
    def show
    end

    # GET /projects/new
    def new
      @project = Ganttproject::Project.new
    end

    # GET /projects/1/edit
    def edit
    end


    # POST /projects
    # POST /projects.json
    def create
      @project = Ganttproject::Project.new(project_params)
      @project.sector_id = params[:sector_id].to_i
      respond_to do |format|
        if @project.save
          sector = ::Sector.find(params[:sector_id])
          sector.master_project_id = @project.id
          sector.save
          @project.process_project_file
          format.html { redirect_to @project, notice: 'Proyecto fue creado satifactoriamente.' }
          format.json { render nothing: true }
        else
          format.html { render :new }
          format.json { render json: @project.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /projects/1
    # PATCH/PUT /projects/1.json
    def update
      respond_to do |format|
        if @project.update(project_params)
          format.html { redirect_to @project, notice: 'Proyecto fue actualizado satifactoriamente.' }
          format.json { render :show, status: :ok, location: @project }
        else
          format.html { render :edit }
          format.json { render json: @project.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /projects/1
    # DELETE /projects/1.json
    def destroy
      @project.destroy
      respond_to do |format|
        format.html { redirect_to projects_url, notice: 'Proyecto fue eliminado satifactoriamente.' }
        format.json { head :no_content }
      end
    end



    def datagantt
      @project = Project.find(params[:id])
      @tasks = @project.tasks.order(id: :asc)
      @links = @project.links
      render :json => {
                 :data =>
                     @tasks.map do |task|
                        task.json_data
                      end.compact,

                 :links =>
                     @links.map do |link|
                       link.json_data
                     end

             }, status: :ok

    end


    def datagantt_action


      params['ids'].split(',').each do |id|
        @id   = params["#{id}_id"].to_i
        @mode = params["#{@id}_!nativeeditor_status"]
        @gantt_mode = params['gantt_mode']

        case @gantt_mode
          when "tasks"
            case @mode
              when "inserted"
                task = Task.new
                task.from_params(params, @id)
                if task.save
                  @tid = task.id
                end

              when "deleted"
                task = Task.find(@id)
                task.destroy
                @tid= @id

              when "updated"
                task = Task.find(@id)
                task.from_params(params, @id)
                if task.save
                  @tid = @id
                end
            end
          when "links"
            case @mode
              when "inserted"
                link = Link.new
                link.from_params(params, session, @id)
                if link.save
                  @tid = link.id
                end

              when "deleted"
                link = Link.find(@id)
                link.destroy
                @tid = @id

              when "updated"
                link = Link.find(@id)
                link.from_params(params, session, @id)
                if link.save
                  @tid = @id
                end
            end
        end
      end

    end



    def datagrid
      @projects = Project.all

      render :json => {
                 :total_count => @projects.length,
                 :pos => 0,
                 :rows => @projects.map do |project|
                   {
                       :id => project.id,
                       :data => [project.name,
                                 project.file_name,
                                 project.resources.count,
                                 project.tasks.count,
                                 "<a href='/ganttproject/projects/"+project.id.to_s+"'>Ver</a>
                                 <a href='/ganttproject/projects/"+project.id.to_s+"/edit'>Editar</a>"
                                #"<form action='/ganttproject/projects/"+project.id.to_s+"' method='get' class='button_to'><input type='submit' value='Ver'></form>"
                       ]
                   }
                 end
             }
    end

    def datagrid_action
      @mode = params["!nativeeditor_status"]
      name = params["c0"]
      @id = params["gr_id"]

      case @mode
        when "inserted"
          project = Project.create :name => name
          if project.save
            @tid = project.id
          end
        when "deleted"
          Project.find(@id).destroy
          @tid = @id

        when "updated"
          project = Project.find(@id)
          project.name = name
          project.save
          @tid = @id
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_project
        @project = Ganttproject::Project.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def project_params
        params.require(:project).permit(:name, :file)
      end
  end
end
