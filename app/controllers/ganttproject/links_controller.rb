require_dependency "ganttproject/application_controller"

module Ganttproject
  class LinksController < ApplicationController
    before_action :set_link, only: [:show, :edit, :update, :destroy]
    before_action :get_project

    def get_project
      @project = Project.find(params[:project_id])
    end

    # GET /links
    # GET /links.json
    def index
      @links = @project.links.all
    end

    # GET /links/1
    # GET /links/1.json
    def show
      @link = @project.links.find(params[:id])
    end

    # GET /links/new
    def new
      @link = @project.links.new()
    end

    # GET /links/1/edit
    def edit
    end

    # POST /links
    def create
      @link = Link.new(link_params)

      if @link.save
        redirect_to @link, notice: 'Link was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /links/1
    def update
      if @link.update(link_params)
        redirect_to @link, notice: 'Link was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /links/1
    def destroy
      @link.destroy
      redirect_to links_url, notice: 'Link was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_link
        @link = Link.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def link_params
        params.require(:link).permit(:source, :target, :gtype, :project_id)
      end
  end
end
