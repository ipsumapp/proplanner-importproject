require "ganttproject/engine"

module Ganttproject
	class Configuration
  end
  class << self
    attr_writer :configuration
  end

  module_function
	  def configuration
	    @configuration ||= Configuration.new
	  end

	  def configure
	    yield(configuration)
	  end
end
