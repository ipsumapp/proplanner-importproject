module Ganttproject
	require 'mpxj'
  require 'ancestry'
  class Engine < ::Rails::Engine
    isolate_namespace Ganttproject

    # For automatic run migration when call rake db:migrate in the rails app wrap
    initializer :append_migrations do |app|
      unless app.root.to_s.match root.to_s
        config.paths["db/migrate"].expanded.each do |expanded_path|
          app.config.paths["db/migrate"] << expanded_path
        end
      end
    end
  end
end