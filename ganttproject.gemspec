$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "ganttproject/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ganttproject"
  s.version     = Ganttproject::VERSION
  s.authors     = ["Juan Patricio Ross I."]
  s.email       = ["prossit@gmail.com"]
  s.homepage    = "http://www.ipsumapp.co"
  s.summary     = "Summary of Ganttproject."
  s.description = "Description of Ganttproject."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails"
  s.add_dependency 'mpxj', '~> 5.1.4'
  s.add_dependency "ancestry"
  s.add_dependency 'sass-rails'

  s.add_development_dependency "sqlite3"

  s.add_dependency 'paper_trail'

end